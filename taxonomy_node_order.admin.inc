<?php
/**
 * @file
 * Taxonomy Node Order page.
 */
 
/**
 * Returns HTML for a term nodes overview form as a sortable list of nodes.
 *
 * @param $variables
 *   An associative array containing:
 *   - form: A render element representing the form.
 *
 * @see taxonomy_node_order_overview_nodes()
 * @ingroup themeable
 */
function theme_taxonomy_node_order_overview_nodes($variables) {
  $form = $variables['form'];

  $errors = form_get_errors() != FALSE ? form_get_errors() : array();
  $rows = array();

  $page = isset($_GET['page']) ? $_GET['page'] : 0;

  $index = 0;
  foreach (element_children($form) as $key) {
    if (isset($form[$key]['#node'])) {
      $node = &$form[$key];

      $row = array();
      $row[] = drupal_render($node['view']);
      $node['weight']['#attributes']['class'] = array('node-weight');
      $row[] = drupal_render($node['weight']);
      $row[] = drupal_render($node['operations']['show']);
      $row[] = drupal_render($node['operations']['edit']);
      $row = array('data' => $row);
      $rows[$key] = $row;
      $rows[$key]['class'][] = 'draggable';
    }

    $index++;
  }

  foreach ($rows as $key => $row) {
    // Add an error class if this row contains a form error.
    foreach ($errors as $error_key => $error) {
      if (strpos($error_key, $key) === 0) {
        $rows[$key]['class'][] = 'error';
      }
    }
  }

  if (empty($rows)) {
    $rows[] = array(array(
        'data' => $form['#empty_text'],
        'colspan' => '4',
      ));
  }

  $header = array(t('Name'), t('Weight'), array(
      'data' => t('Operations'),
      'colspan' => '2',
    ));
  $output = theme('table', array('header' => $header, 'rows' => $rows, 'attributes' => array('id' => 'taxonomy-node-order')));
  $output .= drupal_render_children($form);
  $output .= theme('pager');

  drupal_add_tabledrag('taxonomy-node-order', 'order', 'sibling', 'node-weight');

  return $output;
}

/**
 * Form builder for the taxonomy nodes term overview.
 *
 * Display a tree of all the terms in a vocabulary, with options to edit
 * each one. The form is made drag and drop by the theme function.
 *
 * @ingroup forms
 * @see taxonomy_node_order_overview_nodes_submit()
 * @see theme_taxonomy_node_order_overview_nodes()
 */
function taxonomy_node_order_overview_nodes($form, &$form_state, $term) {
  global $user, $pager_page_array, $pager_total, $pager_total_items;

  // Check for confirmation forms.
  if (isset($form_state['confirm_reset_default'])) {
    return taxonomy_node_order_confirm_reset_default($form, $form_state, $term->tid);
  }

  $form['#tree'] = TRUE;
  $form['#term'] = $term;
  $form['tid'] = array(
    '#type' => 'hidden',
    '#value' => $term->tid,
  );

  $delta = 0;
  $nodes_weight = taxonomy_node_order_select_nodes_weight($term->tid);
  $nids = taxonomy_select_nodes($term->tid, FALSE, FALSE, $order = array('t.weight' => 'ASC', 't.sticky' => 'DESC', 't.created' => 'DESC'));
  $nodes = node_load_multiple($nids);

  foreach ($nodes as $key => $node) {
    $key = 'nid:' . $node->nid . ':' . $delta;

    $form[$key]['#node'] = $node;
    $form[$key]['view'] = array(
      '#type' => 'link',
      '#title' => $node->title,
      '#href' => "node/$node->nid",
    );
    $form[$key]['nid'] = array(
      '#type' => 'hidden',
      '#value' => $node->nid,
    );
    $form[$key]['weight'] = array(
      '#type' => 'weight',
      '#title_display' => 'invisible',
      '#title' => t('Weight of the node'),
      '#default_value' => $nodes_weight[$node->nid],
    );
    $form[$key]['operations']['show'] = array(
      '#type' => 'link',
      '#title' => t('view'),
      '#href' => 'node/' . $node->nid,
      '#options' => array('query' => drupal_get_destination()),
    );

    if (node_access('update', $node, $user)) {
      $form[$key]['operations']['edit'] = array(
        '#type' => 'link',
        '#title' => t('edit'),
        '#href' => 'node/' . $node->nid . '/edit',
        '#options' => array('query' => drupal_get_destination()),
      );
    }
  }

  $form['#empty_text'] = t('There is currently no content classified with this term.');

  if (count($nodes) > 1) {
    $form['actions'] = array(
      '#type' => 'actions',
      '#tree' => FALSE,
    );
    $form['actions']['submit'] = array(
      '#type' => 'submit',
      '#value' => t('Save'),
    );
    $form['actions']['reset_default'] = array(
      '#type' => 'submit',
      '#value' => t('Reset to default'),
    );
    $form_state['redirect'] = array($_GET['q'], (isset($_GET['page']) ? array('query' => array('page' => $_GET['page'])) : array()));
  }

  return $form;
}

/**
 * Submit handler for term nodes order overview form.
 *
 * Rather than using a textfield or weight field, this form depends entirely
 * upon the order of form elements on the page to determine new weights.
 *
 * Because there might be hundreds or thousands of taxonomy terms that need to
 * be ordered, nodes are weighted from 0 to the number of nodes associated to
 * term, rather than the standard -10 to 10 scale. Numbers are sorted
 * lowest to highest, but are not necessarily sequential. Numbers may be skipped
 * when a term has children so that reordering is minimal when a child is
 * added or removed from a term.
 *
 * @see taxonomy_node_order_overview_nodes()
 */
function taxonomy_node_order_overview_nodes_submit($form, &$form_state) {
  // Reset to default
  if ($form_state['clicked_button']['#value'] == t('Reset to default')) {
    // Execute the reset action.
    if ($form_state['values']['reset_default'] === TRUE) {
      return taxonomy_node_order_confirm_reset_default_submit($form, $form_state);
    }
    // Rebuild the form to confirm the reset action.
    $form_state['rebuild'] = TRUE;
    $form_state['confirm_reset_default'] = TRUE;
    return;
  }

  // Sort node order based on weight.
  uasort($form_state['values'], 'drupal_sort_weight');
  $weight = 0;

  $tid = $form_state['values']['tid'];
  $nodes_weight = taxonomy_node_order_select_nodes_weight($tid);

  foreach ($form_state['values'] as $id => $data) {
    if (is_array($data) && isset($data['weight'])) {
      if ($nodes_weight[$data['nid']] != $weight) {
        db_update('taxonomy_index')->fields(array('weight' => $weight))->condition('nid', $data['nid'])->condition('tid', $tid)->execute();
      }
      $weight++;
    }
  }

  drupal_set_message(t('The configuration options have been saved.'));
}

/**
 * Form builder to confirm resetting a term nodes order to default order.
 *
 * @ingroup forms
 * @see taxonomy_vocabulary_confirm_reset_default_submit()
 */
function taxonomy_node_order_confirm_reset_default($form, $form_state, $tid) {
  $term = taxonomy_term_load($tid);

  $form['type'] = array(
    '#type' => 'value',
    '#value' => 'term',
  );
  $form['tid'] = array(
    '#type' => 'value',
    '#value' => $tid,
  );
  $form['name'] = array(
    '#type' => 'value',
    '#value' => $term->name,
  );
  $form['reset_default'] = array(
    '#type' => 'value',
    '#value' => TRUE,
  );

  return confirm_form($form, t("Are you sure you want to reset the term's %title nodes order to default order ?", array('%title' => $term->name)), 'taxonomy/term/' . $term->tid . '/node_order', t('Resetting a term will discard all custom ordering and sort nodes by default.'), t('Reset to default'), t('Cancel'));
}

/**
 * Submit handler to reset the term's nodes order 
 * to defaults after confirmation.
 *
 * @see taxonomy_node_order_confirm_reset_default()
 */
function taxonomy_node_order_confirm_reset_default_submit($form, &$form_state) {
  db_update('taxonomy_index')->fields(array('weight' => 0))->condition('tid', $form_state['values']['tid'])->execute();
  drupal_set_message(t('Reset term %name nodes order to default order.', array('%name' => $form_state['values']['name'])));
  watchdog('taxonomy node order', 'Reset term %name to default order.', array('%name' => $form_state['values']['name']), WATCHDOG_NOTICE);
  $form_state['redirect'] = 'taxonomy/term/' . $form_state['values']['tid'] . '/node_order';
}
