*************************************************************
    README.txt for taxonomy_node_order.module for Drupal
*************************************************************
The 'taxonomy node order' module allows you to order the 
nodes associated with a term by affecting a weight 
to each node.

This weight will be added to the default sorts criteria 
of Drupal (date of creation, sticky node).

By activating the module a new tab appears on the term page. 
By clicking on the tab you get the list of nodes associated 
with the term, you can then order by drag and drop.

A button lets you return to the default sorting.

This module is largely based on 'Node order' module, 
that is not ported to Drupal 7.

Warning: the 'taxonomy_node_order' module uses 
the 'taxonomy_index' table to store the weight of the nodes 
associated with terms, and the variable 
'taxonomy_maintain_index_table' must be TRUE 
for the module to work.

The taxonomy_node_order module was developed by PROBESYS.
